include site.mk
include .make/context.mk
include .make/templates/*
include .make/commands/hosts.mk
include .make/commands/admin.mk
include .make/commands/folders.mk
include .make/commands/mysql.mk

.make/targets/local-provision-laravel:
	@echo ----$@
	$(eval $(local-context))
	$(make-backup-dirs)
	$(ssh) "$(laravel-create-project-cmd)"
	$(ssh) "mkdir $(context)laravel"
	$(ssh) "mv $(context)$(client)/* $(context)laravel/"
	$(make-project-dirs)
ifeq ($(admin-present),present)
	$(ssh) "mkdir $(context)public_html/admin"
	$(ssh) "touch $(context)public_html/admin/.gitkeep"
endif
	$(ssh) "mv $(context)laravel/public/* $(context)public_html/"
	$(ssh) "rm -rf $(context)$(client)"
	$(ssh) "chmod u+x $(context)laravel/artisan"
	touch $@

.PHONY: clean-local-files
clean-local-files:
	@echo ----$@
	rm .make/targets/local-config-files ||:

local-config-files: .make/targets/local-config-files

.make/targets/local-config-files: .make/templates/*
	@echo ----$@
	echo "$$HTACCESS_FILE" > ./public_html/.htaccess
	echo "$$INDEX_FILE" > ./public_html/index.php
	echo "$$LOCAL_ENV_FILE" > ./laravel/.env
	echo "$$NGINX_SITE_FILE" > ~/src/laradock/nginx/sites/$(client).conf
	echo "$$LARAVEL_WEBPACK_TEMPLATE" > ./laravel/webpack.mix.js
	grep -q -F "$(hosts-entry)" ~/src/hosts/myhosts || echo "$(hosts-entry)" >> ~/src/hosts/myhosts
	# python ~/src/hosts/updateHostsFile.py --auto --replace
	# cd ~/src/laradock && docker-compose down
	# cd ~/src/laradock && docker-compose up -d nginx mysql
	touch $@

.make/targets/local-provision-db:
	@echo ----$@
	$(eval $(local-context))
	$(eval db-username=root)
	$(eval db-password=root)
	touch $(backup-folder)/database.sql
	$(mysql-create-db)
	touch $@

.make/targets/live-provision-dirs:
	@echo ----$@
	$(eval $(live-context))
	$(make-project-dirs)
	$(ssh) "rm $(context)public_html/index.html ||:"
	touch $@

.PHONY: backup
ifeq ($(live-host-status),000)
backup: .make/targets/local-provision-laravel .make/targets/local-provision-db
	@echo No live site to backup
else
backup: .make/targets/local-provision-laravel .make/targets/local-provision-db .make/targets/live-provision-dirs
	@echo ----$@
	$(eval $(live-context))
	$(ssh) "$(mysql-mail-dump-cmd)"
	$(sync) $(account):$(context)public_html $(backup-folder)
	$(mysql-dump-to-backup-file)
endif

.PHONY: local-clear-caches
local-clear-caches:
	@echo ----$@
	$(eval $(local-context))
	$(ssh) "$(clear-all-caches-cmd)"

.PHONY: local-reset-db-setup
local-reset-db-setup: local-sync .make/targets/local-provision-db
	@echo ----$@
	$(eval $(local-context))
	$(eval db-username=root)
	$(eval db-password=root)
	$(mysql-clean-db)

.PHONY: local-reset-db-from-live
local-reset-db-from-live: local-reset-db-setup
	@echo ----$@
	$(mysql-restore-from-backup)
	$(migrate)

.PHONY: local-reset-db-from-seed
local-reset-db-from-seed: local-reset-db-from-live
	@echo ----$@
	$(seed)

.PHONY: local-seed
local-seed:
	@echo ----$@
	$(eval $(local-context))
	$(migrate)
	$(seed)

.PHONY: local-sync
local-sync: backup local-clear-caches .make/targets/local-provision-db .make/targets/local-config-files
	@echo ----$@
	$(eval $(live-context))
	$(sync) $(backup-folder)/public_html/uploads/ ~/src/$(client)/public_html/uploads/

.PHONY: local-sync-files-from-live
local-sync-files-from-live: local-sync
	@echo ----$@
	$(eval $(live-context))
	$(sync) $(account):$(context)laravel/resources/ ~/src/$(client)/laravel/resources/

.PHONY: clean-build-admin
clean-build-admin:
	@echo ----$@
	rm -rf .make/targets/build-admin ||:

.make/targets/build-admin: $(wildcard ./admin/src/**/*) ./admin/package.json
	@echo ----$@
	cd ./admin && PUBLIC_URL=$(app-path)/admin yarn build
	$(sync) ./admin/build/ ./public_html/admin-assets
	touch $@

.make/targets/optimize-autoloader: laravel/composer.json laravel/composer.lock
	@echo ----$@
	$(eval $(local-context))
	$(ssh) "$(composer) install --optimize-autoloader"
	touch $@

.PHONY: deploy-staging
ifeq ($(admin-present),present)
deploy-staging: export REACT_APP_ENV = staging
deploy-staging: local-sync clean-build-admin .make/targets/build-admin .make/targets/optimize-autoloader
else
deploy-staging: local-sync
endif
	@echo ----$@
	$(eval $(staging-context))
	$(make-project-dirs)
	$(sync-no-delete) ./laravel/ $(account):$(context)laravel/
	$(sync-no-delete) ./public_html/ $(account):$(context)public_html/
ifeq ($(admin-present),present)
	$(deploy-admin-view)
endif
	echo "$$STAGING_ENV_FILE" | $(ssh) 'cat > $(context)laravel/.env'
	echo '<?php header("Location: ./public_html/");' | $(ssh) 'cat > $(context)index.php'
	$(ssh) "$(clear-all-caches-cmd) && $(create-all-caches-cmd)"
	$(migrate)

ifeq ($(live-host-status),000)
.PHONY: deploy-staging-clean-db
deploy-staging-clean-db: deploy-staging
	@echo ----$@
	$(eval $(staging-context))
	$(mysql-clean-db)
	$(migrate)
	$(seed)
else
.PHONY: deploy-staging-clean-db
deploy-staging-clean-db: deploy-staging
	@echo ----$@
	$(eval $(staging-context))
	$(mysql-clean-db)
	$(mysql-restore-from-backup)
	$(migrate)
endif

.PHONY: deploy-live
ifeq ($(admin-present),present)
deploy-live: export REACT_APP_ENV = production
deploy-live: local-sync clean-build-admin .make/targets/build-admin .make/targets/optimize-autoloader
else
deploy-live: local-sync .make/targets/optimize-autoloader
endif
	@echo ----$@
	$(eval $(live-context))
	$(make-project-dirs)
	$(sync-no-delete) ./laravel/ $(account):~/laravel/
	$(sync-no-delete) ./public_html/ $(account):~/public_html/
	echo "$$PROD_ENV_FILE" | $(ssh) 'cat > ~/laravel/.env'
	$(ssh) "$(clear-all-caches-cmd) && $(create-all-caches-cmd)"
	$(migrate)
ifeq ($(admin-present),present)
	$(deploy-admin-view)
endif

.PHONY: composer-update
composer-update: local-sync
	@echo ----$@
	$(eval $(local-context))
	$(ssh) "$(composer) update"

.PHONY: local-permissions
local-permissions:
	@echo ----$@
	find . -type d $(find-excludes) -exec sudo chmod 755 {} \;
	find . -type f $(find-excludes) -exec sudo chmod 644 {} \;
	find ./laravel/storage -type d $(find-excludes) -exec sudo chmod 775 {} \;
	find ./laravel/storage -type f $(find-excludes) -exec sudo chmod 664 {} \;
	sudo chown -R :daemon ./laravel/storage

.PHONY: live-ssh
live-ssh:
	@echo ----$@
	$(eval $(live-context))
	$(ssh)

.PHONY: staging-ssh
staging-ssh:
	@echo ----$@
	$(eval $(staging-context))
	$(ssh)
