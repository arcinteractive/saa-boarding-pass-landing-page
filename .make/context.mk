context = ./

check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
        $(error Undefined env var $1$(if $2, ($2))$(if $(value @), \
                required by target $@)))

$(call check_defined, CLIENT, client name; as a URL and filename compatible string)
$(call check_defined, SITE_TITLE, website title; a full string title with no restrictions)
$(call check_defined, SITE_USER, shared hosting username; the Hetzner username and home directory for live hosting)

define live-context
  app-path = $(LIVE_APP_PATH)
	db-username = $(LIVE_DB_USERNAME)
	db-password = $(LIVE_DB_PASSWORD)
	db-host = $(LIVE_DB_HOST)
	db-database = $(LIVE_DB_DATABASE)
	context = ./
	account = $$(user)@$$(domain)
  ssh = $$(ssh-cmd) $$(account)
	mysql-ssh = $$(ssh)
endef

define staging-context
  app-path = $(STAGING_APP_PATH)
	db-username = $(STAGING_DB_USERNAME)
	db-password = $(STAGING_DB_PASSWORD)
	db-host = $(STAGING_DB_HOST)
	db-database = $(STAGING_DB_DATABASE)
	context = ~/public_html/$(client)/
	account = testlqxqfj@testlink.co.za
  ssh = $$(ssh-cmd) $$(account)
	mysql-ssh = $$(ssh)
endef

define local-context
  app-path = $(LOCAL_APP_PATH)
	db-username = $(LOCAL_DB_USERNAME)
	db-password = $(LOCAL_DB_PASSWORD)
	db-host = $(LOCAL_DB_HOST)
	db-database = $(LOCAL_DB_DATABASE)
	context = ./$(client)/
	ssh = cd ~/src/laradock && docker-compose exec workspace bash -c
	mysql-ssh = cd ~/src/laradock && docker exec -i $$$$(docker-compose ps -q mysql) bash -c
  # mysql-clean = $$(mysql-ssh) "$$(mysql-clean-db-root-cmd)"
endef
