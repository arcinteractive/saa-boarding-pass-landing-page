define LARAVEL_WEBPACK_TEMPLATE
let mix = require('laravel-mix');

/*
  |--------------------------------------------------------------------------
  | Mix Asset Management
  |--------------------------------------------------------------------------
  |
  | Mix provides a clean, fluent API for defining some Webpack build steps
  | for your Laravel application. By default, we are compiling the Sass
  | file for the application as well as bundling up all the JS files.
  |
*/

mix.setPublicPath('../public_html')
    .options({ processCssUrls: false });

mix.js('resources/assets/js/app.js', 'js')
    .sass('resources/assets/sass/app.sass', 'css');
endef
export LARAVEL_WEBPACK_TEMPLATE
