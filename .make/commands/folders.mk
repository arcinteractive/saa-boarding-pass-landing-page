backup-folder ?= ~/$(BACKUP_LOCATION)/clients/$(client)/live-backup
make-backup-dirs = mkdir -p $(backup-folder)/public_html/uploads

laravel-project-dirs = $(context){laravel/{bootstrap/cache,storage/{logs,app,search,framework/{sessions,views,cache}}},public_html/uploads}

make-project-dirs-cmd = mkdir -p $(laravel-project-dirs)
make-project-dirs = $(ssh) '$(make-project-dirs-cmd)'

rsync-cmd := rsync -rvhazL -e "$(ssh-cmd)"
rsync-excludes := --exclude=storage/ --exclude=logs/ --exclude=node_modules/
sync := $(rsync-cmd) $(rsync-excludes) --delete-excluded
sync-no-delete :=  $(rsync-cmd) $(rsync-excludes) --exclude=.env

artisan = php7.0 $(context)laravel/artisan

clear-all-caches-cmd = \
    $(artisan) cache:clear && \
    $(artisan) config:clear && \
    $(artisan) clear-compiled && \
    $(artisan) route:clear && \
    $(artisan) view:clear

create-all-caches-cmd = \
    $(artisan) route:cache && \
    $(artisan) config:cache

seed = $(ssh) "$(artisan) db:seed --force"

composer = composer --working-dir='$(context)laravel'

laravel-create-project-cmd = composer create-project --working-dir='$(context)' --prefer-dist laravel/laravel $(client) '5.5.*'

find-excludes := -not -path "*.git/*" -not -path "*vendor*" -not -path "*uploads*"

