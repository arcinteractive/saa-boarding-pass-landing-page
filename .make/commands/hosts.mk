host-status-cmd := curl -o /dev/null --silent --head --write-out '%{http_code}'
live-host-status := $(shell $(host-status-cmd) $(LIVE_APP_URL))

hosts-entry = 127.0.0.1 $(LOCAL_APP_URL)

ssh-cmd = ssh
ssh = $(ssh-cmd) $(account)

