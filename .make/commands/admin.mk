admin-present := $(shell if [ -d "./admin" ]; then echo "present"; fi)

ifeq ($(admin-present),present)
asset-filename = sed -n 's|\.*".*$1": "\(.*\)".*|admin-assets/\1|pg' public_html/admin-assets/asset-manifest.json
else
asset-filename =
endif

deploy-admin-view = echo "$$ADMIN_BLADE_TEMPLATE" | $(ssh) 'cat > $(context)laravel/resources/views/admin.blade.php'
